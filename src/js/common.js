"use strict";
$(document).ready(function() {
	document.createElement( "picture" );
	$(document).on("click", function() {
		$(".js-toggle-search").removeClass("is-active");
		$(".js-search").removeClass("is-active");
	});

	var videoSlider = {
		init: function() {
			this.$videoSlider = $(".js-video-slider");
			this.$videoSliderBody = $(".js-video-slider-body");
			this.$dot = $(".js-slider-dots button");
			this.firstVideo = document.getElementById("0");
			this._initSlider();
			this._bindEvent();
		},
		_initSlider: function() {
			this.$videoSliderBody.slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				dots: false,
				prevArrow: ".js-prev-slide",
				nextArrow: ".js-next-slide"
			});
		},
		_bindEvent: function() {
			if (this.firstVideo) {
				this.firstVideo.addEventListener("canplay", this._startSlider.bind(this));
				this.firstVideo.addEventListener("ended", this._nextSlide.bind(this));
			}
			else {
				this._startSlider();
			}
			this.$videoSliderBody.on("beforeChange", this._beforeChangeSlide.bind(this));
			this.$dot.on("click", this._goToSlide.bind(this));
		},
		_startSlider: function() {
			this._showSlider();
			if (this.firstVideo) {
				this._startFirstVideo();
			}			
		},
		_startFirstVideo: function() {
			this.firstVideo.play();
		},
		_showSlider: function() {
			this.$videoSlider.addClass("is-ready");
		},
		_nextSlide: function() {
			$(".js-video-slider-body").slick("next");
		},
		_beforeChangeSlide: function(event, slick, currentSlide, nextSlide) {
			console.log("next: "+nextSlide+" currentSlide: "+currentSlide);
			this._stopCurrentVideo(currentSlide, nextSlide);
			this._playNextVideo(currentSlide, nextSlide);
			this._changeDot(nextSlide);
		},
		_stopCurrentVideo: function(currentSlide, nextSlide) {
			var videoCurrent = document.getElementById(currentSlide);
			if(videoCurrent) videoCurrent.pause();
		},
		_playNextVideo: function(currentSlide, nextSlide) {
			var videoNext = document.getElementById(nextSlide);
			if(videoNext) {
				videoNext.play();
				videoNext.addEventListener("ended", this._nextSlide);
			}

		},
		_goToSlide: function(event) {
			this._active = $(event.currentTarget);
			this._activeIndex = this._active.data("index");
			this.$videoSliderBody.slick("slickGoTo", this._activeIndex);
			this._changeDot(this._activeIndex);
			event.stopPropagation();
		},
		_changeDot: function(index) {
			$(".js-slider-dots li").removeClass("slick-active");
			$(".js-slider-dots li").eq(index).addClass("slick-active");
		}
	};
	videoSlider.init();

	var toggle = {
		init: function(options) {
			this.$btn = $(options.btn);
			this.$focusInput;
			this.focusInput = options.focus;
			this._bindEvent();
		},
		_bindEvent: function() {
			this.$btn.on("click", this._toggleTarget.bind(this));
		},
		_toggleTarget: function(event) {
			var $el = $(event.currentTarget);
			var $target = $("."+$el.data("toggle"));
			if ($el.data("toggle") !== "parent") {
				$target.toggleClass("is-active");
				$el.toggleClass("is-active");
			}
			else {
				$el.parents(".js-parent").toggleClass("is-active");
				$el.toggleClass("is-active");
			}
			
			return false;
		}
	};
	toggle.init({
		btn: ".js-toggle"
	});
	toggle.init({
		btn: ".js-toggle-search",
		focus: ".js-search input"
	});

	var shower = {
		init: function(btn) {
			this.$btn = $(btn);
			this._bindEvent();
		},
		_bindEvent: function() {
			this.$btn.on("click", this._toggleClass.bind(this));
		},
		_toggleClass: function(event) {
			var $el = $(event.currentTarget);
			var parentClass = $el.data("class");
			var $parent = $($el.data("parent"));
			$parent.toggleClass(parentClass);
			$el.toggleClass("is-active");
			event.stopPropagation();
		}
	};
	shower.init(".js-toggle-class");

	var focusInput = {
		init: function(input) {
			this.$input = $(input);
			this._focus();
		},
		_focus: function() {
			this.$input.focus();
		}
	}

	$(".js-search").on("click", function(event) {
		event.stopPropagation();
	});
	var SelectList = {
		init: function(options) {
			this._el = document.querySelectorAll(options.selector);
			this._parentClass = options.cssClass;
			this._body = document.body;
			this._makeDom();
			this._firstLoad();
			this._onClick();
		},
		_makeDom: function() {
			this._createParent();
			this._createList();
			this._createTitle();
		},
		_createParent: function() {
			for (var i = 0; i < this._el.length; i++) {
				var parent = document.createElement("div");
				this._el[i].parentNode.insertBefore(parent, this._el[i]);
				parent.appendChild(this._el[i])
				parent.classList.add(this._parentClass);
				parent.classList.add("sssl");
			}
			this._parent = document.querySelectorAll("." + this._parentClass);
		},
		_createList: function() {
			for (var i = 0; i < this._parent.length; i++) {
				var options = this._parent[i].querySelectorAll("option");
				var ul = document.createElement('ul');
				for (var j = 0; j < options.length; j++) {
					var li = "<li>" + options[j].innerHTML + "</li>";
					ul.insertAdjacentHTML("beforeEnd", li);
				}
				this._parent[i].appendChild(ul);
				ul.classList.add(this._parentClass + "__list");
			}
			this._ul = document.querySelectorAll(this._parentClass + " ul");

		},
		_createTitle: function() {
			for (var i = 0; i < this._parent.length; i++) {
				var title = document.createElement("span");
				title.classList.add(this._parentClass + "__head");
				this._parent[i].appendChild(title);
			}
			this._title = document.querySelectorAll(this._parentClass + " span");
		},
		_onClick: function() {
			for (var i = 0; i < this._parent.length; i++) {
				this._parent[i].addEventListener("click", this._clickAction.bind(this));
			}
            document.addEventListener("click", this._hideList.bind(this));
		},
		_clickAction: function(event) {
			if (event.currentTarget.children[0].disabled) {
				return false;
			}
			console.dir(event.currentTarget);
			if (event.target.tagName == "SPAN") {
				this._toggleList(event);
			}
			if (event.target.tagName == "LI") {
				this._changeVal(event);
			}
            event.stopPropagation();
		},
		_toggleList: function(event) {
			event.currentTarget.classList.toggle("is-active");
		},
        _hideList: function(event) {
            var select = document.querySelectorAll(".sssl");
            for (var i = 0; i < select.length; i++) {
				select[i].classList.remove("is-active");
			}
		},
		_changeVal: function(event) {
			var arrLi = event.currentTarget.querySelectorAll("li");
			var arrOptions = event.currentTarget.querySelectorAll("option");
			var target = event.target;
			var parent = event.currentTarget;
			var index = [].indexOf.call(arrLi, target);
			for (var j = 0; j < arrOptions.length; j++) {
				if (index == j) {
					arrOptions[j].selected = true;
					break;
				}
			}
			this._changeTitle(parent, arrLi[index].innerHTML);
			this._toggleList(event);
		},
		_changeTitle: function(parent, text) {
			parent.querySelector("span").innerHTML = text;
		},
		_firstLoad: function() {
			for (var i = 0; i < this._parent.length; i++) {
				var options = this._parent[i].querySelectorAll("option");
				var li = this._parent[i].querySelectorAll("li");
				var arrLi = [].slice.call(li);
				var title = this._parent[i].querySelector("span");
				for (var j = 0; j < options.length; j++) {
					var option = options[j];
					if (option.selected) {
						var optionIndex = j;
						break;
					}
				}
				title.innerHTML = arrLi[optionIndex].innerHTML;
			}
		}
	};
	SelectList.init({
		selector: ".js-select1",
		cssClass: "select"
	});

	var scrollTo = {
		init: function(options) {
			this.$el = $(".js-scroll-to");
			this.$root = $('html, body');
			this._bindEvents();
		},
		_bindEvents: function() {
			this.$el.on("click", this._goTo.bind(this));
		},
		_goTo: function(event) {
			var $el = $(event.currentTarget)
			var hash = $el.attr("href");
			var $target = $(hash);
			var offset;
			if ($el.data("offset")) {
				offset = +$el.data("offset");
			}
			else {
				offset = 0;
			}
			this.$root.animate({
			    scrollTop: $target.offset().top + offset 
			}, 500);
			window.location.hash = hash;
			event.stopPropagation();
			return false;
		}
	};
	scrollTo.init();

	var scroller = {
		init: function(options) {
			this.$el = $(options.selector);
			this.$elHeight = this.$el.outerHeight();
			this.$root = $('html, body');
			this.$win = $(window);
			this.$target = $(".js-scroll-target");
			this.timeout = 500;
			this._bindEvents();
		},
		_bindEvents: function() {
			this.$el.on("click", this._goTo.bind(this));
			this.$win.scroll(this._changeActiveTarget.bind(this));
		},
		_goTo: function(event) {
			var $el = $(event.currentTarget)
			var hash = $el.attr("href");
			if (hash.length > 1) {
				var $target = $(hash);
				var offset;
				if ($el.data("offset")) {
					offset = +$el.data("offset");
				}
				else {
					offset = 45;
				}
				
				this.$root.animate({
				    scrollTop: $target.offset().top
				}, this.timeout);
				setTimeout(function() {
					window.location.hash = hash;
				}, this.timeout)
				event.preventDefault();
				return false;
			}
			
		},
		_changeActiveTarget: function() {
			var _this = this;
			console.log("work")
			this.$target.each(function() {
				if (+_this.$win.scrollTop() + _this.$elHeight >= $(this).offset().top) {
					var id = $(this).attr("id");
					_this.$el.removeClass("is-active");
					$('[href="#'+id+'"]').addClass("is-active");
				}
			});

		}
	};
	scroller.init({
		selector: ".js-scroller a"
	});

	var stickyBar = {
		init: function(selector) {
			this.$el = $(selector);
			this.elTop = this.$el.offset().top;
			this.$win = $(window);
			this.$root = $("html");
			this._bindEvents();
		},
		_bindEvents: function() {
			this.$win.scroll(this._sticky.bind(this));
		},
		_sticky: function() {
			if (this.$win.scrollTop() >= this.elTop) {
				this.$root.addClass("is-fixed-navigation");
			}
			else {
				this.$root.removeClass("is-fixed-navigation");
			}
		}

	};
	if ($(".js-fixed-navigation").length) {
		stickyBar.init(".js-fixed-navigation");
	}
	

	var overviewGallery = {
		init: function() {
			this.$btn = $(".js-change-slide");
			this.$slider = $(".js-crsl-demo");
			this._bindEvent();
		},
		_bindEvent: function() {
			this.$btn.on("click", this._btnClick.bind(this));
			this.$slider.on("beforeChange", this._changeActiveBtn.bind(this));
		},
		_btnClick: function(event) {
			this.$activeBtn = $(event.currentTarget);
			this.index = this.$activeBtn.data("index");
			this.$slider = $('[data-helper='+this.$activeBtn.data("slider")+']');
			this.$otherBtns = $('[data-slider='+this.$activeBtn.data("slider")+']');
			this._changeSlide(this.$slider, this.index);
			this._changeBtnState(this.$activeBtn, this.$otherBtns);
			return false;
		},
		_changeSlide: function(slider, index) {
			slider.slick("slickGoTo", index);
		},
		_changeBtnState: function(activeBtn, otherBtns) {
			otherBtns.removeClass("is-active");
			activeBtn.addClass("is-active");
		},
		_changeActiveBtn: function(event, slick, currentSlide, nextSlide) {
			var helper = slick.$slider.data("helper");
			$('[data-slider="'+helper+'"]').removeClass("is-active");	
			$('[data-slider="'+helper+'"][data-index="'+nextSlide+'"]').addClass("is-active");	
		}
	};
	overviewGallery.init();
	
	var car360 = {
		init: function() {
			this.$btnColor = $(".js-change-color");
			this.btnColorActive = ".js-change-color.is-active";
			this.$car = $('.js-car-360');
			this.path = this.$car.data("path");
			this.firstCarColor = this.$car.data("color");
			this.$btnPrev = $('.js-prev-color');
			this.$btnNext = $('.js-next-color');
			this._initCar();
			this._bindEvents();
		},
		_initCar: function() {
			var _this = this;
			if (this.$car.length) {
				this.$car.ThreeSixty({
					totalFrames: 36,
					endFrame: 35,
					currentFrame: 0,
					zeroBased: true,
					imgList: '.js-car-list',
					progress: '.js-spinner',
					imagePath: _this.path + _this.firstCarColor + "/",
					filePrefix: '',
					ext: '.png',
					height: 488,
					width: 732,
					navigation: true,
					responsive: true
				});
			}
			
		},
		_bindEvents: function() {
			this.$btnColor.on("click", this._clickAction.bind(this));
			this.$btnPrev.on("click", this._prevColor.bind(this));
			this.$btnNext.on("click", this._nextColor.bind(this));
		},
		_clickAction: function(event) {
			this.$active = $(event.currentTarget);
			this._changeColor();
			this._changeBtnState();
			event.stopPropagation();
		},
		_changeColor: function() {
			var color = this.$active.data("color");
			this.$images = $(".js-car-list img");
			var srcStr;
			var srcArr;
			this.$images.each(function() {
				srcArr = $(this).attr("src").split("/");
				srcArr[srcArr.length-2] = color;
				srcStr = srcArr.join("/");
				$(this).attr("src", srcStr);
			});
		},
		_changeBtnState: function() {
			this.$btnColor.removeClass("is-active");
			this.$active.addClass("is-active");
		},
		_prevColor: function(event) {
			this.$btnColorActive = $("body").find(".js-change-color.is-active");
			if (this.$btnColorActive.prev()[0]) {
				this.$btnColorActive.prev().trigger("click");
			}
			else {
				this.$btnColor.last().trigger("click");
			}
			event.stopPropagation();
		},
		_nextColor: function(event) {
			this.$btnColorActive = $("body").find(".js-change-color.is-active");
			if (this.$btnColorActive.next()[0]) {
				this.$btnColorActive.next().trigger("click");
			}
			else {
				this.$btnColor.first().trigger("click");
			}
			event.stopPropagation();
		}
	};
	car360.init();

	var sliderTrigger = {
		init: function(options) {
			this.$slider = $(options.selector);
			this.btnSelector = ".js-slide-trigger";
			this.$btn = $(this.btnSelector);
			//this.$content = this.$group.find(".js-slider-content");
			this._bindEvents();
		},
		_bindEvents: function() {
			this.$slider.on("beforeChange", this._changeTrigger.bind(this));
			this.$btn.on("click", this._changeSlide.bind(this));
		},
		_changeTrigger: function(event, slick, currentSlide, nextSlide) {
			this.$group = $("."+slick.$slider.data("group"));
			var $btn = this.$group.find(".js-slide-trigger");
			this.$content = this.$group.find(".js-slider-content");
			var indexNext = nextSlide;
			var indexCurrent = currentSlide;
			$btn.eq(indexCurrent).removeClass("is-active");
			$btn.eq(indexNext).addClass("is-active");

			this._changeContent(indexCurrent, indexNext);
		},
		_changeContent: function(indexCurrent, indexNext) {
			console.log(this.$content.eq(indexCurrent))
			this.$content.eq(indexCurrent).removeClass("is-active");
			this.$content.eq(indexNext).addClass("is-active");
			var $crsl = this.$content.eq(indexNext).find(".slick-slider");
			if ($crsl.length) {
				$crsl[0].slick.setPosition();
			}
		},
		_changeSlide: function(event) {
			var $active = $(event.currentTarget);
			var index = $active.index();
			var slider = $active.data("slider");
			$("."+slider).slick("slickGoTo", index);

		}

	};
	sliderTrigger.init({
		selector: ".js-crsl-cars",
		hiddenSlider: false
	});
	sliderTrigger.init({
		selector: ".js-crsl-main",
		hiddenSlider: true,
		hiddenSliderSelector: ".js-crsl-details"
	});

	var showTolltip = {
		init: function() {
			this.$el = $(".js-show-tooltip");
			this._bindEvents();
		},
		_bindEvents: function() {
			this.$el.on("click", this._changeState.bind(this));
		},
		_changeState: function(event) {
			var $active = $(event.currentTarget);
			if ($active.hasClass("is-active")) {
				$active.removeClass("is-active");
			}
			else { 
				this.$el.removeClass("is-active");
				$active.addClass("is-active");
			}

			
			
		}
	}
	showTolltip.init();

	var toggleCollapse = {
		init: function() {
			this.$btn = $(".js-toggle-collapse");
			this._bindEvents();
		},
		_bindEvents: function() {
			this.$btn.on("click", this._toogle.bind(this));
		},
		_toogle: function(event) {
			var $active = $(event.currentTarget);
			var $collapse = $("."+$active.data("collapse"));
			var collapsed = $active.data("collapsed");
			if (collapsed) {
				$collapse.addClass("in");
				$collapse.prev().removeClass("collapsed");
			}
			else {
				$collapse.removeClass("in");
				$collapse.prev().addClass("collapsed");
			}
			event.stopPropagation();
			
			
		}
	}
	toggleCollapse.init();

	var customCollapse = {
		init: function() {
			this.$btn = $(".js-custom-collapse");
			this._bindEvents();
		},
		_bindEvents: function() {
			this.$btn.on("click", this._toogle.bind(this));
		},
		_toogle: function(event) {
			var $active = $(event.currentTarget);
			var $collapse = $("."+$active.data("collapse"));
			$collapse.slideToggle(200).toggleClass("is-active");
			$active.toggleClass("is-active");
			event.stopPropagation();
		}
	}
	customCollapse.init();
	
	var makeDisabled = {
		init: function() {
			this.$el = $(".js-make-disable");
			this._bindEvents();
		},
		_bindEvents: function() {
			this.$el.on("change", this._toogle.bind(this));
		},
		_toogle: function(event) {
			var $active = $(event.currentTarget);
			var $target = $("."+$active.data("target"));
			if ($active.is(":checked")) {
				$target.attr("disabled", false).focus();
			}
			else {
				$target.attr("disabled", true).val("");
				if ($target.parent().find(".select__head").length) {
					$target.parent().find(".select__head").text("");
				}
			}
		}
	}
	makeDisabled.init();

	var popup = {
		init: function() {
			this.$btnOpen = $(".js-popup-trigger");
			this.$btnClose = $(".js-popup-close");
			this.$popup = $(".js-popup");
			this.$root = $("html");
			this._bindEvents();
		},
		_bindEvents: function() {
			this.$btnOpen.on("click", this._showPopup.bind(this));
			this.$btnClose.on("click", this._closePopup.bind(this));
		},
		_showPopup: function(event) {
			var $active = $(event.currentTarget);
			var $popup = $("."+$active.data("popup"));
			this.$root.addClass("is-open-popup");
			$popup.addClass("is-active");
			return false;
		},
		_closePopup: function(event) {
			var $active = $(event.currentTarget);
			this.$root.removeClass("is-open-popup");
			this.$popup.removeClass("is-active");
			return false;
		}
	}
	popup.init();
});
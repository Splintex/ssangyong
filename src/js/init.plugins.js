"use strict";
$(document).ready(function() {
 
	$(".js-crsl-demo").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		dots: true,
		arrows: true,
		adaptiveHeight: true
	});

	$(".js-crsl-overview").on('init', function(event, slick, direction){
		setTimeout(function(){
			$(".js-crsl-overview").addClass("is-ready");
		},200)
	});

	$(".js-crsl-overview").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		arrows: true,
		fade: true,
		asNavFor: '.js-crsl-overview-nav'

	});

	$(".js-crsl-overview-nav").on('init', function(event, slick, direction){
		setTimeout(function(){
			$(".js-crsl-overview-nav").addClass("is-ready");
		},200)
	});

	$(".js-crsl-overview-nav").slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: true,
		focusOnSelect: true,
		asNavFor: '.js-crsl-overview',
		responsive: [
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			}
		]
	});

	var crslHistory = $('.js-crsl-history');
	crslHistory.on('init', function(event, slick, direction){
		setTimeout(function(){
			crslHistory.addClass("is-ready");
		}, 200)
	});
	crslHistory.slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: false,	
		fade: true,
		arrows: true,
		asNavFor: '.js-crsl-history-nav',
		adaptiveHeight: true,
		prevArrow: ".js-history-prev",
		nextArrow: ".js-history-next"
	});
	crslHistory.on('beforeChange', function(event, slick, currentSlide, nextSlide){
	  console.log(nextSlide);
	  crslHistoryNav.find('.slick-slide').removeClass("slick-current");
	  crslHistoryNav.find('[data-slick-index="'+nextSlide+'"]').addClass("slick-current");
	});
       
    var crslHistoryNav = $('.js-crsl-history-nav'); 
	crslHistoryNav.slick({
		slidesToShow: 15,
		slidesToScroll: 1,
		infinite: false,
		arrows: false,
		asNavFor: '.js-crsl-history',
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 10,
					slidesToScroll: 5
				}
			},
			{
				breakpoint: 1025,
				settings: {
					slidesToShow: 6,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			}
		]
	});


	$(".js-tabs a").on("click", function(event) {
		$(this).tab('show');
		var id = $(this).attr("href");
		var $crsl = $(id).find(".js-crsl-demo");
		if ($crsl.length)
			$crsl[0].slick.setPosition();
		event.preventDefault();
	});

	$(".js-crsl-main").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		arrows: false
	});

	var crslDetailsSet = {
		slidesToShow: 3,
		slidesToScroll: 3,
		arrows: true,
		dots: true,
		infinite: false,
		adaptiveHeight: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 416,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	}
	$(".js-crsl-details").slick(crslDetailsSet);

	var crslCars = $(".js-crsl-cars");
	crslCars.slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		arrows: true,
		prevArrow: '<button type="button" class="btn-nav btn-nav_light is-prev"></button>',
		nextArrow: '<button type="button" class="btn-nav btn-nav_light"></button>'
	});
	$(".js-crsl-info").slick({
		slidesToScroll: 1,
		infinite: false,
		slidesToShow: 1,
		fade: true,
		arrows: true,
		dots: true,
		adaptiveHeight: true,
		prevArrow: '<button type="button" class="btn-nav is-prev"></button>',
		nextArrow: '<button type="button" class="btn-nav"></button>',
	});
	$(".js-crsl-wall").on('init', function(event, slick, direction){
		setTimeout(function(){
			$(".js-crsl-wall").addClass("is-ready");
		},200)
	});
	$('.js-crsl-wall').slick({
		slidesPerRow: 4,
		rows: 2,
		arrows: true,
		responsive: [
			{
				breakpoint: 960,
				settings: {
					slidesPerRow: 3,
				}
			},
			{
				breakpoint: 769,
				settings: {
					slidesPerRow: 2,
				}
			},
			{
				breakpoint: 380,
				settings: {
					slidesPerRow: 1,
				}
			}
		]
	});

	$('.js-crsl-video').slick({
		slidesToShow: 1,
		slidesToScroll: 1,	
		fade: true,
		arrows: false,
		asNavFor: '.js-crsl-video-nav',
		adaptiveHeight: true
	});
	$('.js-crsl-video-nav').slick({
		slidesToShow: 6,
		slidesToScroll: 6,
		arrows: true,
		dots: true,
		infinite: false,
		asNavFor: '.js-crsl-video',
		focusOnSelect: true,
		adaptiveHeight: true,
		prevArrow: '<button type="button" class="btn-nav is-prev"></button>',
		nextArrow: '<button type="button" class="btn-nav"></button>',
		responsive: [
		   {
				breakpoint: 1025,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4,
				}
			},
			{
				breakpoint: 960,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
				}
			},
			{
				breakpoint: 769,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				}
			}
		]
	});
	$(".js-crsl-video-nav .slick-slide").on("click", function() {
		$(".js-crsl-video-nav .slick-slide").removeClass("is-active");
		$(this).addClass("is-active");
	});

	$('.js-crsl-news').slick({
		slidesToShow: 3,
		slidesToScroll: 3,
		arrows: true,
		dots: true,
		infinite: false,
		adaptiveHeight: true,
		prevArrow: '<button type="button" class="btn-nav is-prev"></button>',
		nextArrow: '<button type="button" class="btn-nav"></button>',
		responsive: [
			{
				breakpoint: 769,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			}
		]
	});

	$(".js-fancybox").fancybox({
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		helpers : {
			overlay: {
				locked: false
			},
			title: {
				type: 'outside',
				position: 'bottom'
			}
		},
		nextEffect: 'fade',
		prevEffect: 'fade',
		openEffect: 'fade',
		closeEffect: 'fade'
	});

	$('.js-masonry').masonry({
		itemSelector: '.js-masonry-item',
		columnWidth: '.js-masonry-sizer',
		gutter: 0,
		percentPosition: true
	});
	

});
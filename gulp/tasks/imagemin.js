var gulp = require('gulp');
var config = require('../config');
var imgIcons = config.src.img+'icons/*.*';
var imgSvg = config.src.img+'svg/*.*';
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');

// minify images
gulp.task('imagemin', function() {
   gulp.src([config.src.img+'**/*.*', '!'+imgIcons, '!'+imgSvg])
   .pipe(imagemin({
       progressive: true,
       svgoPlugins: [{removeViewBox: false}],
       use: [pngquant()]
   }))
   .pipe(gulp.dest(config.dest.img));
});

"use strict";
(function() {
	function initPanorama() {
		var params = {
			quality: "high",
			bgcolor: "#fff",
			allowscriptaccess: "sameDomain",
			allowfullscreen: "true",
			base: "."
		};
		var attributes = {
			id: "pano",
			name: "pano",
			align: "middle"
		};
		swfobject.embedSWF(
			"js/lib/swf/black_navi_out.swf", "panorama-black", 
			"680", "304", 
			"9.0.0", "expressInstall.swf", 
			params, attributes);
		swfobject.embedSWF(
			"js/lib/swf/beige_navi_out.swf", "panorama-beige", 
			"680", "304", 
			"9.0.0", "expressInstall.swf", 
			params, attributes);
		swfobject.embedSWF(
			"js/lib/swf/red_navi_out.swf", "panorama-red", 
			"680", "304", 
			"9.0.0", "expressInstall.swf", 
			params, attributes);
		swfobject.embedSWF(
			"js/lib/swf/black_audio_out.swf", "panorama-black-audio", 
			"680", "304", 
			"9.0.0", "expressInstall.swf", 
			params, attributes);
		swfobject.embedSWF(
			"js/lib/swf/beige_audio_out.swf", "panorama-beige-audio", 
			"680", "304", 
			"9.0.0", "expressInstall.swf", 
			params, attributes);
		swfobject.embedSWF(
			"js/lib/swf/red_audio_out.swf", "panorama-red-audio", 
			"680", "304", 
			"9.0.0", "expressInstall.swf", 
			params, attributes);
	};
	initPanorama();
	

})();